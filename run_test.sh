#!/bin/bash

set -x

OUTPUT_DIR="./output"
TEST_DATA="./bin"

KRITA_BIN="${TEST_DATA}/krita/bin/krita"
TEST_SCRIPT="${1}"
TEST_FILE="${2}"

# env setup
krita_usage_log=~/.local/share/krita.log
mkdir -p "${OUTPUT_DIR}/"

export QT_LINUX_ACCESSIBILITY_ALWAYS_ON=1 # enables dogtail
# export QT_LOGGING_RULES="*.*=false;krita*=true" # debug logging for krita
export QT_LOGGING_RULES="*=true" # debug logging for krita

# defines
function log() {
  local message="${1}"
  local d=$(date +'%x %X')
  echo "${d} :: ${message}"
}

# system setup
#sudo apt install krita python-dogtail gnome-shell-extension-no-annoyance
#gnome-shell-extension-tool -e "noannoyance@sindex.com"
#log "system set up"

for i in $(seq 1 20); do
  log "initiated test run ${i}"
  # test env setup
  tmpdir=$(mktemp -p "${OUTPUT_DIR}" -d "krita_saving_test_XXXXXXXX")
  cp "${TEST_FILE}" "${tmpdir}/"
  cp "${TEST_SCRIPT}" "${tmpdir}/testcase.py"
  > $krita_usage_log

  log "$tmpdir :: test env set up"

  pushd .
  #run the test
  cd $tmpdir

  log "$tmpdir :: running ui automation"
  ./testcase.py #>> krita_output.log 2>&1

# check for save file; TODO: the file may also be too old
# if [ -f "./race.kra" ]; then
#   log "$tmpdir :: save file exists, trying to unzip"
#   # try unzipping
#   unzip "race.kra" -p | > /dev/null
#   unzip_ret=$?
#
#   if [ ${unzip_ret} -ne 0 ]; then
#     log "$tmpdir :: unzipping save file failed, it may be corrupt"
#   else
#     log "$tmpdir :: save file successfully unzipped"
#   fi
# else
#   log "$tmpdir:: save file does not exist"
# fi

# collect artifacts
  mv "${krita_usage_log}" ./
  mv ${HOME}/*.webm ./
  # mv ${HOME}/bin/core ./
  log "$tmpdir :: artifacts collected"

  # log "$tmpdir :: end of test run ${i}"

  popd
done
