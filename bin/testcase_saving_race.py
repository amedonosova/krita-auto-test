#!/usr/bin/env python3

from dogtail.utils import isA11yEnabled, enableA11y
if isA11yEnabled() is False:
    enableA11y(True)

import os
import errno
import time
from signal import signal, alarm, SIGALRM, SIGKILL
from subprocess import Popen

from dogtail.rawinput import keyCombo, absoluteMotion, pressKey, click
from dogtail.tree import root
from dogtail.utils import run, doDelay
from dogtail.predicate import GenericPredicate

def take_screenshot(step_name):
    try:
        os.system("import -w root '" + os.getcwd() + "/screenshot_" + step_name + ".png'")
    except Exception as e:
        print("Cannot take screenshot: %s" % e)

# class taken from the Gnome project
class App(object):
    """
    This class does all basic events with the app
    """
    def __init__(
        self, appName, shortcut='<Control><Q>', a11yAppName=None,
            forceKill=True, parameters='', recordVideo=False):
        """
        Initialize object App
        appName     command to run the app
        shortcut    default quit shortcut
        a11yAppName app's a11y name is different than binary
        forceKill   is the app supposed to be kill before/after test?
        parameters  has the app any params needed to start? (only for startViaCommand)
        recordVideo start gnome-shell recording while running the app
        """
        self.appCommand = appName
        self.shortcut = shortcut
        self.forceKill = forceKill
        self.parameters = parameters
        self.internCommand = self.appCommand.lower()
        self.a11yAppName = a11yAppName
        self.recordVideo = recordVideo
        self.pid = None

        # a way of overcoming overview autospawn when mouse in 1,1 from start
        pressKey('Esc')
        absoluteMotion(100, 100, 2)

    def isRunning(self):
        """
        Is the app running?
        """
        if self.a11yAppName is None:
            self.a11yAppName = self.internCommand

        # Trap weird bus errors
        for attempt in range(0, 10):
            try:
                return self.a11yAppName in [x.name for x in root.applications()]
            except GLib.GError:
                continue
        raise Exception("10 at-spi errors, seems that bus is blocked")

    def kill(self):
        """
        Kill the app via 'killall'
        """
        try:
            os.kill(self.pid, SIGKILL)
        except:
            # Fall back to killall
            Popen("killall " + self.appCommand, shell=True).wait()

    def startViaCommand(self):
        """
        Start the app via command
        """
        if self.forceKill and self.isRunning():
            self.kill()
            assert not self.isRunning(), "Application cannot be stopped"

        command = "%s %s" % (self.appCommand, self.parameters)
        self.pid = run(command, timeout=10)

        assert self.isRunning(), "Application failed to start"
        return root.application(self.a11yAppName)

    def closeViaShortcut(self):
        """
        Close the app via shortcut
        """
        if not self.isRunning():
            raise Exception("App is not running")

        keyCombo(self.shortcut)
        assert not self.isRunning(), "Application cannot be stopped"


if __name__ == "__main__":
    keyCombo('<Ctrl><Alt><Shift>R') # start recording

# 1: start and load test file
    krita_app_class = App('bash /home/tilya/bin/run_krita.sh', parameters='master race.kra', a11yAppName='krita')
    krita = krita_app_class.startViaCommand()
    time.sleep(10)
    take_screenshot("1_loaded")

# 2: make a change
    #TODO: find the position of the center of the canvas
    absoluteMotion(1000, 500, 2)
    # switch to fill tool
    keyCombo("F")
    # fill layer
    click(1000, 500)

    time.sleep(20) # give it time to draw
    take_screenshot ("2_make_a_change")

# 3: save
    # os.system("callgrind_control -i on")
    keyCombo("<ctrl>S")
    # no sleeping time here, somewhere in here is the race condition!
    # take_screenshot ("3_save")

# 4: quickly close via shortcut
    keyCombo("<alt><F4>")

# 5: wait until app is closed
    time.sleep(60) # give it a minute to finish saving; it takes 30 secs to initiate saving in the testcase + some padding

    if krita_app_class.isRunning():
        time.sleep(60)
        krita_app_class.kill()

    keyCombo('<Ctrl><Alt><Shift>R') # stop recording
